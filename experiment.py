import pandas as pd
import os
import csv
import argparse
from iocbio.kinetics.handler.experiment_generic import ExperimentGeneric
from iocbio.kinetics.constants import database_table_experiment


IocbioKineticsModule = ["database_schema"]

class ExperimentPlaceholder(ExperimentGeneric):
    
    database_table = "placeholder"


    @staticmethod
    def database_schema(db):
        db.query("CREATE TABLE IF NOT EXISTS " + db.table(ExperimentPlaceholder.database_table) +
                 "(experiment_id text PRIMARY KEY, rawdata text, filename text, title text, " +
                 "FOREIGN KEY (experiment_id) REFERENCES " + db.table(database_table_experiment) + "(experiment_id) ON DELETE CASCADE" +
                 ")")

    def full_txt(database, experiment_id, o2kchamber):
        if ExperimentGeneric.hardware(database, experiment_id) != "Oroboros":
            return none
        print("Loading raw data for experiment:", experiment_id, ", Chamber:", o2kchamber)
        data = pd.read_csv(database, encoding = "ISO-8859-1")
        return data



    #generating a list from the data for neccesary operations
    #data_list = list(data.columns)
   # data_titles = ["Time [min]", "Event Name"]
   # for i in data_list:
    #    if i.find(o2kchamber) != -1:
      #      data_titles.append(i)
     #   return data[data_titles]
#    return None




    def store(database, data):
        experiment_id = data.experiment_id

        ExperimentGeneric.store(database,experiment_id, time=data.time,
                                type_generic = data.type_generic,
                                type_specific = data.type, hardware= "Oroboros")

        c = database

        if not database.has_record(ExperimentPlaceholder.database_table, experiment_id = experiment_id):
            c.query("INSERT INTO " + database.table(ExperimentPlaceholder.database_table) +
                   "(experiment_id, rawdata, filename, title) " +
                   "VALUES(:experiment_id,:ftxt,:fname,:exptitle)",
                   experiment_id=experiment_id,
                   ftxt=fulltxt,
                   fname=data.config["this_file"],
                   exptitle=data.config["Experiment name"])


# definition of database_schema function
def database_schema(db):
    # just in case if our experiment is the first one, make general if needed
    ExperimentGeneric.database_schema(db)
    # create table for our experiment
    ExperimentPlaceholder.database_schema(db)
