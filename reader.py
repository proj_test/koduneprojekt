import hashlib
import pandas as pd
import csv
from iocbio.kinetics.handler.experiment_generic import ExperimentGeneric
from pandas.util import hash_pandas_object
from iocbio.kinetics.io.data import Data
from .experiment import ExperimentPlaceholder

IocbioKineticsModule = ["reader","args"]

def args(parser):
    parser.add(name='o2k_chamber', help='The selection of the Chamber of Interest. For example: 4A, 4B')
    return '''Oroboros 2K Protocols:
------------------
O2K - Generic O2K Protocol
'''






def reader(txt,o2kchamber):
    data_list = list(txt.columns)
    data = []
    for i in data_list:
        if i.find(o2kchamber+ ": O2") != -1:
            data.append(i)


    return data_list, data



def create_data(database, experiment_id=None, args= None):
    filename=getattr(args, "file_name", None)
    print(args)
    o2k_chamber = "4A"
    fulltxt = None
    if experiment_id is not None:
        fulltxt = ExperimentPlaceholder.get_fulltxt(database,experiment_id,o2kchamber)

    if filename is not None:
        fulltxt = pd.read_csv(filename, encoding = "ISO-8859-1")

    if fulltxt is None: return None

    x = fulltxt


    if experiment_id is None:
        expid = "Oroboros 2K - " + hashlib.sha256(fulltxt.to_records(index=False).tostring()).hexdigest()
    else:
        expid = experiment_id
    print(type(x))

    import re
    date = str(re.findall(r"\d{4}-\d{2}-\d{2}", filename)).strip("[]")
    print(date)



   # d["this_file"] = filename

   # dd = {"O2K_chamber": {"x": x,
                   #       "y": Carrier("O2K chamber", "um", y)}}
    data = Data( expid,
                 config = None,
                 type = "Oroboros 2K experiment",
                 type_generic = "2K experiment",
                 time = date,
                 name = "Insert Name Here",
                 xname = "Time", xunit = "seconds",
                 xlim = None,
                 data = x)

    if not ExperimentGeneric.has_record(database, data.experiment_id):
        database.set_read_only(False)
        ExperimentPlaceholder.store(fulltxt, data)

    return data
#data = pd.read_csv(database, encoding = "ISO-8859-1")
        ##generating a list from the data for neccesary operations
        #data_list = list(data.columns)
        #data_titles = ["Time [min]", "Event Name"]
        #for i in data_list:
           # if i.find(o2kchamber) != -1:
              #  data_titles.append(i)
